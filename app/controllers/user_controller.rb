class UserController < ApplicationController
  def index
    @admins_posts = UsersService.get_all_admins_with_posts(User)
  end
end

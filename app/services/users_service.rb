module UsersService
  module_function

  def get_all_admins_with_posts(user_model)
    user_model.admin_posts
  end
end

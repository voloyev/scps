class User < ApplicationRecord
  has_many :posts

  scope :admins_posts, -> do
    joins(posts: { theme: :count })
      .includes(posts: { theme: :count })
      .where(users: { admin: true })
  end

  scope :users_posts, -> do
    joins(posts: [{ theme: :count }])
      .where(users: { admin: nil })
      .where(users: {posts: {theme: {counts: {id: 63}}}})
      .includes(posts: { theme: :count })
  end
 end

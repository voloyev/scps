class Count < ApplicationRecord
  belongs_to :theme
  
  scope :all_in_one, -> { joins(theme: { post: :user }) }
end

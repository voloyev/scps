class AwesomeMiddleware
  def initialize(app)
    @app = app
  end

  def call(env)
    @status, @headers, @response = @app.call(env)

    [@status,
     { 'Content-Type' => 'text/html' },
     change_response]
  end

  def change_response
    @response[0] << '<p><em>I am from Awesome middleware</em>'
    @response
  end
end

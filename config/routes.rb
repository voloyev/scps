Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/all_in_one', to: 'all_in_one#index'
  get 'admins_posts', to: 'user#index'
end

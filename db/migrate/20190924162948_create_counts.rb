class CreateCounts < ActiveRecord::Migration[6.0]
  def change
    create_table :counts do |t|
      t.integer :body
      t.references :theme

      t.timestamps
    end
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
20.times do |user_num|
  user = User.create(name: "Bob #{user_num}", age: 22, admin: true)
  20.times do |n|
    post = user.posts.create(body: n)
    post.theme = Theme.create(title: "#{n} theme")
    post.theme.count = Count.create(body: n)
  end

  user_non_admin = User.create(name: "Bob non Admin #{user_num} ", age: 21)

  20.times do |n|
    post_non_admin = user_non_admin.posts.create(body: n*10)
    post_non_admin.theme = Theme.create(title: "#{n*10} theme")
    post_non_admin.theme.count = Count.create(body: n*10)
  end
end
